<?php
/**
 * Created by PhpStorm.
 * User: shutk
 * Date: 03.11.2017
 * Time: 19:50
 */

$largestThreeDig = 999;
$largestPalindrome = 0;
for($l = $largestThreeDig; $l > 0 && $l * $largestThreeDig > $largestPalindrome; $l--) {
    for($p = $l * $largestThreeDig; $p > $largestPalindrome; $p -= $l) {
        if((string)$p === strrev((string)$p)) {
            $largestPalindrome = $p;
        }
    }
}
echo "Largest Palindrome: $largestPalindrome\n";

