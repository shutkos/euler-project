<?php
/**
 * Created by PhpStorm.
 * User: shutk
 * Date: 04.11.2017
 * Time: 18:17
 */

$data = [];
$sum = 0;

for ($i = 0; $i <= 2000000; $i++) {
    $prime = gmp_prob_prime($i);
    if ($prime == 2 or $prime == 1) {
        array_push($data, $i);
    }
}

for($i = 0; $i <= count($data); $i++){
    $sum = $sum + $data[$i];
}

echo $sum;