<?php
/**
 * Created by PhpStorm.
 * User: shutk
 * Date: 03.11.2017
 * Time: 21:55
 */

function sumSquareDiff($input) {
    $sumOfSquares = "";
    $sum = "";
    for($n = 1; $n <= $input; $n++) {
        $square = pow($n, 2);
        $sumOfSquares += $square;
        $sum += $n;
    }
    $squareOfSums = pow($sum, 2);
    $difference = $squareOfSums - $sumOfSquares;
    echo $difference."\n";
}
sumSquareDiff(100);